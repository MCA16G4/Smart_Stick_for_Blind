long duration;
long distance;
int echo = 2;
int trigger = 3;
int buzzer = 4; 
int echo_2 = 6;
int trigger_2 = 7;
int temperature =A0;
int ldr = A2;
int button = 5;
int water = A1;
 void setup()
  {
   pinMode(echo,INPUT);
   pinMode(trigger,OUTPUT);
   pinMode(buzzer,OUTPUT);
   pinMode(echo_2,INPUT);
   pinMode(trigger_2,OUTPUT);
   pinMode(buzzer,OUTPUT);
   pinMode(temperature,INPUT);
   pinMode(water,INPUT);
   pinMode(ldr,INPUT);
   pinMode(button,INPUT);
   Serial.begin(9600);
  }
  void light()
 {  
  while(digitalRead(button)==LOW)
   {
    if(analogRead(ldr) > 500)
     {
      digitalWrite(buzzer,HIGH);
      delay(50);
      digitalWrite(buzzer,LOW);
      delay(50);    
      Serial.println("Light");
     }
    else
     {
      digitalWrite(buzzer,LOW);
     }
   }
 }
void loop()
  {
  /////----Object---/////////
   digitalWrite(trigger,HIGH);
   delayMicroseconds(10);
   digitalWrite(trigger,LOW);
   duration = pulseIn(echo, HIGH);
   distance = duration/58;
   if(distance <= 45)
      {
       digitalWrite(buzzer,HIGH);
       delay(200);
       digitalWrite(buzzer,LOW);
       delay(200);  
       Serial.println("Object");
      }
   else
     {
      digitalWrite(buzzer,LOW);
     }
       /////----Slope---/////////
     digitalWrite(trigger_2,HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger_2,LOW);
  duration = pulseIn(echo_2, HIGH);
  distance = duration/58;
  if(distance > 70)
   {
    digitalWrite(buzzer,HIGH);
    delay(200);
    Serial.println("Slope");
   }
  else
   {
    digitalWrite(buzzer,LOW);
   }
   ////////----Temperature------/////////    
if(analogRead(temperature) > 700)
  {
    digitalWrite(buzzer,HIGH);
    delay(50);
    digitalWrite(buzzer,LOW);
    delay(50);    
   // Serial.print("Temperature =");
  //  Serial.println(analogRead(temperature));
    }
else
  {
    digitalWrite(buzzer,LOW);
    }
  
   ////////----Water------/////////    
  if(analogRead(water) > 500)
   {
    digitalWrite(buzzer,HIGH);
    delay(50);
    digitalWrite(buzzer,LOW);
    delay(200);   
    Serial.println("Water"); 
   }
  else
   {
    digitalWrite(buzzer,LOW);
   }
    
  if(digitalRead(button)==LOW)
   {
    light()

   }  
 }
   } 
